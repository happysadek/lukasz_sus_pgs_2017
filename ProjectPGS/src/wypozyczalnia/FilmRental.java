package wypozyczalnia;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import wypozyczalnia.dao.CustomerDao;
import wypozyczalnia.dao.Dao;
import wypozyczalnia.dao.FilmDao;
import wypozyczalnia.dao.RentalDao;
import wypozyczalnia.model.Customer;
import wypozyczalnia.model.Film;
import wypozyczalnia.model.IRow;
import wypozyczalnia.model.Rental;

/**
 * Klasa z programem glownym - wypozyczalnia filmow.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 */
public class FilmRental extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final String DATA_FILE_NAME = "FilmRental.bin";
	private final static Logger LOGGER = Logger.getLogger(FilmRental.class.getName());
	private Dao filmDao = new FilmDao();
	private Dao customerDao = new CustomerDao();
	private Dao rentalDao = new RentalDao();
	private CollectionViewer cvfilms;
	private CollectionViewer cvcustomers;
	private CollectionViewer cvrentals;

	private JMenuBar initializeMenu() {

		JMenu[] menu = { new JMenu("Pomoc") };

		JMenuItem author = new JMenuItem("Autor");
		JMenuItem description = new JMenuItem("Opis");
		JMenuItem finish = new JMenuItem("Zakoncz");

		author.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Autor programu: �ukasz Sus\nData wykonania: 28.04.2017");
			}

		});

		description.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Oprogramowanie wspomagajace prace wypozyczalni filmow.\n"
						+ "Dzialanie na przykladzie zakladki filmy:\n"
						+ "- \"Znajdz\" filtruje filmy na podstawie wpisanych danych.\n"
						+ "- Aby dodac film musza zostac uzupelnione wszystkie pola poza polem id.\n"
						+ "Id filmu jest generowane automatycznie a pole to nie jest uwzglednione przez przycisk \"dodaj\".\n"
						+ "- Filmy usuwane sa na podstawie id. Inne pola nie sa uwzgledniane przez przycisk \"usun\".");
			}
		});

		finish.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					saveRentalToFile();
				} catch (Exception e1) {
					LOGGER.warning(e1.toString());
				}
				System.exit(0);
			}

		});

		menu[0].add(author);
		menu[0].add(description);
		menu[0].add(finish);

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(menu[0]);

		return menuBar;
	}

	private JPanel initializeFilms() {
		JPanel filmsPanel = new JPanel();
		JLabel idLabel = new JLabel("ID Filmu:");
		JLabel titleLabel = new JLabel("Tytul:");
		JLabel genreLabel = new JLabel("Gatunek");
		JLabel mediaLabel = new JLabel("Nosnik:");
		JTextField idText = new JTextField(10);
		JTextField titleText = new JTextField(10);
		JTextField genreText = new JTextField(10);
		JComboBox<String> mediaCombo = new JComboBox<String>();
		mediaCombo.addItem("DVD");
		mediaCombo.addItem("CD");
		mediaCombo.addItem("BlueRay");
		mediaCombo.addItem("VHS");
		JButton addFilm = new JButton("Dodaj film");
		addFilm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (titleText.getText().isEmpty() || genreText.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Pola \"tytu\" i \"gatunek\" musza byc wypelnione!");
					return;
				}
				IRow film = new Film(titleText.getText(), genreText.getText(), mediaCombo.getSelectedItem().toString());
				filmDao.add(film);
				List<IRow> films = filmDao.getAll();
				cvfilms.printList(films);

			}
		});
		JButton removeFilm = new JButton("Usun film");
		removeFilm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(
						!idText.getText().isEmpty() && idText.getText().matches("[0-9]+") ? idText.getText() : "-1");
				if (id == -1) {
					JOptionPane.showMessageDialog(null, "Pole id musi zawierac nieujemna liczbe!");
					return;
				}
				filmDao.remove(id);
				List<IRow> films = filmDao.getAll();
				cvfilms.printList(films);
			}
		});
		JButton findFilm = new JButton("Znajdz film");
		findFilm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!idText.getText().isEmpty() && !idText.getText().matches("[0-9]+")) {
					JOptionPane.showMessageDialog(null, "Pole id musi zawierac liczbe nieujemna!");
					return;
				}
				int id = Integer.parseInt(!idText.getText().isEmpty() ? idText.getText() : "-1");
				String title = titleText.getText().isEmpty() ? "" : titleText.getText();
				String genre = genreText.getText().isEmpty() ? "" : genreText.getText();
				String media = mediaCombo.getSelectedItem().toString();
				IRow criteria = new Film(id, title, genre, media);
				List<IRow> films = filmDao.find(criteria);
				cvfilms.printList(films);
			}
		});
		JButton getAll = new JButton("Wyswietl wszystkie");
		getAll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				List<IRow> films = filmDao.getAll();
				cvfilms.printList(films);
			}
		});
		String[] column = { "Id", "Tytul", "Gatunek", "Nosnik", "Dostepny" };
		cvfilms = new CollectionViewer("Filmy", 600, 400, column);
		filmsPanel.add(idLabel);
		filmsPanel.add(idText);
		filmsPanel.add(titleLabel);
		filmsPanel.add(titleText);
		filmsPanel.add(genreLabel);
		filmsPanel.add(genreText);
		filmsPanel.add(mediaLabel);
		filmsPanel.add(mediaCombo);
		filmsPanel.add(findFilm);
		filmsPanel.add(addFilm);
		filmsPanel.add(removeFilm);
		filmsPanel.add(getAll);
		filmsPanel.add(cvfilms);

		return filmsPanel;
	}

	private JPanel initializeCustomers() {
		JPanel customersPanel = new JPanel();
		JLabel idLabel = new JLabel("Id:");
		JLabel nameLabel = new JLabel("Imie klienta:");
		JLabel lastNameLabel = new JLabel("Nazwisko klienta:");
		JLabel numberLabel = new JLabel("Numer telefonu:");
		JTextField idText = new JTextField(10);
		JTextField nameText = new JTextField(10);
		JTextField lastNameText = new JTextField(10);
		JTextField numberText = new JTextField(10);
		JButton addCustomer = new JButton("Dodaj klienta");
		addCustomer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (nameText.getText().isEmpty() || lastNameText.getText().isEmpty()
						|| numberText.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null,
							"Pola \"imie\", \"nazwisko\" i \"numer\" musza byc wypelnione!");
					return;
				}
				if (!numberText.getText().matches("[0-9]+")) {
					JOptionPane.showMessageDialog(null, "Wpisz poprawny numer telefonu!");
					return;
				}
				IRow customer = new Customer(nameText.getText(), lastNameText.getText(),
						Integer.parseInt((numberText.getText())));
				customerDao.add(customer);
				List<IRow> customers = customerDao.getAll();
				cvcustomers.printList(customers);
			}
		});
		JButton removeCustomer = new JButton("Usun klienta");
		removeCustomer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(
						!idText.getText().isEmpty() && idText.getText().matches("[0-9]+") ? idText.getText() : "-1");
				if (id == -1) {
					JOptionPane.showMessageDialog(null, "Pole id musi zawierac nieujemna liczbe!");
					return;
				}
				customerDao.remove(id);
				List<IRow> customers = customerDao.getAll();
				cvcustomers.printList(customers);
			}

		});
		JButton findCustomer = new JButton("Znajdz klienta");
		findCustomer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!idText.getText().isEmpty() && !idText.getText().matches("[0-9]+")) {
					JOptionPane.showMessageDialog(null, "Pole id musi zawierac liczbe nieujemna!");
					return;
				}
				if (!numberText.getText().isEmpty() && !numberText.getText().matches("[0-9]+")) {
					JOptionPane.showMessageDialog(null, "Pole \"numer telefonu\" musi zawierac liczbe nieujemna!");
					return;
				}
				int id = Integer.parseInt(!idText.getText().isEmpty() ? idText.getText() : "-1");
				String name = nameText.getText().isEmpty() ? "" : nameText.getText();
				String lastName = lastNameText.getText().isEmpty() ? "" : lastNameText.getText();
				int number = Integer.parseInt(!numberText.getText().isEmpty() ? numberText.getText() : "-1");
				IRow criteria = new Customer(id, name, lastName, number);
				List<IRow> customers = customerDao.find(criteria);
				cvcustomers.printList(customers);
			}

		});
		JButton getAll = new JButton("Wyswietl wszystkich");
		getAll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				List<IRow> customers = customerDao.getAll();
				cvcustomers.printList(customers);
			}
		});
		String[] column = { "Id", "Imie", "Nazwisko", "Nr telefonu" };
		cvcustomers = new CollectionViewer("Klienci", 600, 400, column);
		customersPanel.add(idLabel);
		customersPanel.add(idText);
		customersPanel.add(nameLabel);
		customersPanel.add(nameText);
		customersPanel.add(lastNameLabel);
		customersPanel.add(lastNameText);
		customersPanel.add(numberLabel);
		customersPanel.add(numberText);
		customersPanel.add(findCustomer);
		customersPanel.add(addCustomer);
		customersPanel.add(removeCustomer);
		customersPanel.add(getAll);
		customersPanel.add(cvcustomers);
		return customersPanel;
	}

	private JPanel initializeRentals() {
		JPanel rentalsPanel = new JPanel();
		JLabel idLabel = new JLabel("Id:");
		JLabel idCustomerLabel = new JLabel("ID Clienta:");
		JLabel idFilmLabel = new JLabel("ID Filmu:");
		JTextField idText = new JTextField(10);
		JTextField idCustomerText = new JTextField(10);
		JTextField idFilmText = new JTextField(10);
		JButton addRental = new JButton("Dodaj wypozyczenie");
		addRental.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (idCustomerText.getText().isEmpty() || idFilmText.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Pola \"id kleinta\" i \"id filmu\" musza byc wypelnione!");
					return;
				}
				if (!idCustomerText.getText().matches("[0-9]+") || !idFilmText.getText().matches("[0-9]+")) {
					JOptionPane.showMessageDialog(null, "Numery id musza byc liczbami nieujemnymi!");
					return;
				}
				int customer = Integer.parseInt(idCustomerText.getText());
				int film = Integer.parseInt(idFilmText.getText());
				if (filmDao.getOne(film) == null || customerDao.getOne(customer) == null) {
					JOptionPane.showMessageDialog(null, "Nie znaleziono klienta lub filmu!");
					return;
				}
				if (!((Film)filmDao.getOne(film)).isAvailable()){
					JOptionPane.showMessageDialog(null, "Film jest wypozyczony!");
					return;
				}
				IRow rental = new Rental(customer, film, new Date());
				rentalDao.add(rental);
				((Film)filmDao.getOne(film)).setAvailability(false);
				List<IRow> rentals = rentalDao.getAll();
				cvrentals.printList(rentals);
				List<IRow> films = filmDao.getAll();
				cvfilms.printList(films);
			}

		});
		JButton finishRental = new JButton("Zakoncz wypozyczenie");
		finishRental.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(
						!idText.getText().isEmpty() && idText.getText().matches("[0-9]+") ? idText.getText() : "-1");
				if (id == -1) {
					JOptionPane.showMessageDialog(null, "Pole id musi zawierac nieujemna liczbe!");
					return;
				}
				Rental r = (Rental) rentalDao.getOne(id);
				if (r != null) {
					r.setDateOfReturn(new Date());
					((Film) filmDao.getOne(r.getIdFilm())).setAvailability(true);
				} else {
					JOptionPane.showMessageDialog(null, "Nie znaloziono wypozyczenia!");
					return;
				}
				List<IRow> rentals = rentalDao.getAll();
				cvrentals.printList(rentals);
				List<IRow> films = filmDao.getAll();
				cvfilms.printList(films);
			}

		});
		JButton getAll = new JButton("Wyswietl wszystkie");
		getAll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				List<IRow> rentals = rentalDao.getAll();
				cvrentals.printList(rentals);
			}

		});
		JButton getFinished = new JButton("Wyswietl zakonczone");
		getFinished.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				List<IRow> rentals = new LinkedList<IRow>();
				((RentalDao) rentalDao).getFinished(rentals);
				cvrentals.printList(rentals);
			}

		});
		JButton getNotFinished = new JButton("Wyswietl niezakonczone");
		getNotFinished.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				List<IRow> rentals = new LinkedList<IRow>();
				((RentalDao) rentalDao).getNotFinished(rentals);
				cvrentals.printList(rentals);
			}

		});
		String[] column = { "Id", "Id Klienta", "Id Filmu", "Data Wypozyczenia", "Data zwrotu" };
		cvrentals = new CollectionViewer("Wypozyczenia", 600, 400, column);
		rentalsPanel.add(idLabel);
		rentalsPanel.add(idText);
		rentalsPanel.add(idCustomerLabel);
		rentalsPanel.add(idCustomerText);
		rentalsPanel.add(idFilmLabel);
		rentalsPanel.add(idFilmText);
		rentalsPanel.add(addRental);
		rentalsPanel.add(finishRental);
		rentalsPanel.add(getAll);
		rentalsPanel.add(getFinished);
		rentalsPanel.add(getNotFinished);
		rentalsPanel.add(cvrentals);
		return rentalsPanel;
	}

	private void saveRentalToFile() throws Exception {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(DATA_FILE_NAME));
		out.writeObject(filmDao);
		out.writeObject(customerDao);
		out.writeObject(rentalDao);
		out.writeObject(Film.getLastId());
		out.writeObject(Customer.getLastId());
		out.writeObject(Rental.getLastId());
		out.close();
	}

	private void loadRentalFromFile() throws Exception {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(DATA_FILE_NAME));
		filmDao = (FilmDao) in.readObject();
		customerDao = (CustomerDao) in.readObject();
		rentalDao = (RentalDao) in.readObject();
		Film.setLastId((int) in.readObject());
		Customer.setLastId((int) in.readObject());
		Rental.setLastId((int) in.readObject());
		in.close();
	}

	public FilmRental() {
		super("Wypozyczalnia filmow");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent e) {
				try {
					saveRentalToFile();
				} catch (Exception e1) {
					LOGGER.warning(e.toString());
				}
			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}

		});
		setJMenuBar(initializeMenu());
		setSize(700, 700);

		// initialize tabbedPane
		JTabbedPane tabbedpane = new JTabbedPane();
		JPanel filmsPanel = initializeFilms();
		JPanel customersPanel = initializeCustomers();
		JPanel rentalsPanel = initializeRentals();
		tabbedpane.addTab("Filmy", filmsPanel);
		tabbedpane.addTab("Klienci", customersPanel);
		tabbedpane.addTab("Wypozyczenia", rentalsPanel);

		setContentPane(tabbedpane);

		try {
			loadRentalFromFile();
		} catch (Exception e) {
			LOGGER.warning(e.toString());
		}

		setVisible(true);
	}

	public static void main(String[] args) {
		new FilmRental();
	}

}