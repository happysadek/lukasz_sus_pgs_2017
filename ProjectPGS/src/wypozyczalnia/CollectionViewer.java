package wypozyczalnia;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import wypozyczalnia.dao.Dao;
import wypozyczalnia.model.IRow;

/**
 * Klasa rozbudowywuje JScrollPane tak, aby wyswietlac liste elementow klasy implementujacej interface IRow.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 */
class CollectionViewer extends JScrollPane {

	private static final long serialVersionUID = 1L;
	private JTable table;
	private DefaultTableModel tableModel;
	
	CollectionViewer(String name, int width, int height, String[] column) {
		tableModel = new DefaultTableModel(column, 0);
		table = new JTable(tableModel);
		table.setRowSelectionAllowed(false);
		setViewportView(table);
		setPreferredSize(new Dimension(width, height));
		setBorder(BorderFactory.createTitledBorder(name));
	}

	void printList(List<IRow> list) {
		tableModel.setRowCount(0);
		for (IRow c : list) {
			String[] row = c.getRow();
			tableModel.addRow(row);
		}
	}

}