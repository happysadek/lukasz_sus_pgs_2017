package wypozyczalnia.model;

/**
 * Klasa stanowi model klienta wypozyczalni.
 *  
 * @author �ukasz Sus
 * @version 3.05.2017
 */
public class Customer implements IRow{
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String lastName;
	private int phoneNum;
	
	private static int LAST_ID = 0;

	private static int generateID() {
		LAST_ID += 1;
		return LAST_ID;
	}
	
	public Customer(String name, String lastName, int phoneNum){
		super();
		this.id = generateID();
		this.phoneNum = phoneNum;
		this.name = name;
		this.lastName = lastName;
	}
	
	public Customer(int id, String name, String lastName, int phoneNum){
		super();
		this.id = id;
		this.phoneNum = phoneNum;
		this.name = name;
		this.lastName = lastName;
	}
	
	public static int getLastId(){
		return LAST_ID;
	}
	
	public static void setLastId(int lastId){
		LAST_ID=lastId;
	}
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public int getNumber(){
		return phoneNum;
	}
	
	public void setNum(int number){
		phoneNum = number;
	}

	@Override
	public String[] getRow() {
		return new String[] { Integer.toString(getId()), getName(), getLastName(), Integer.toString(getNumber()) };
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", lastName=" + lastName + ", phoneNum=" + phoneNum + "]";
	}
}
