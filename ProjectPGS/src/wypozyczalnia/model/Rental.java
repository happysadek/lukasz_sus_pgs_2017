package wypozyczalnia.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Klasa stanowi model wypozyczenia filmu.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 */
public class Rental implements IRow {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private int idCustomer;
	private int idFilm;
	private Date dateOfRental;
	private Date dateOfReturn;
	
	private static int LAST_ID = 0;

	private static int generateID() {
		LAST_ID += 1;
		return LAST_ID;
	}

	public Rental(int customer, int film, Date dateOfRental) {
		super();
		id = generateID();
		idCustomer = customer;
		idFilm = film;
		this.dateOfRental = dateOfRental;
		this.dateOfReturn = null;
	}
	
	public Rental(int id, int customer, int film, Date dateOfRental, Date dateOfReturn) {
		super();
		this.id = id;
		idCustomer = customer;
		idFilm = film;
		this.dateOfRental = dateOfRental;
		this.dateOfReturn = dateOfReturn;
	}

	public static int getLastId(){
		return LAST_ID;
	}
	
	public static void setLastId(int lastId){
		LAST_ID=lastId;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int number) {
		id = number;
	}

	public int getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(int number) {
		idCustomer = number;
	}

	public int getIdFilm() {
		return idFilm;
	}

	public void setIdFilm(int number) {
		idFilm = number;
	}

	public Date getDateOfRental() {
		return dateOfRental;
	}

	public void setDateOfRental(Date date) {
		dateOfRental = date;
	}

	public Date getDateOfReturn() {
		return dateOfReturn;
	}

	public void setDateOfReturn(Date date) {
		dateOfReturn = date;
	}

	@Override
	public String[] getRow() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String dor;
		if(dateOfReturn==null)
			dor = "";
		else
			dor = sdf.format(dateOfReturn);
		return new String[] { Integer.toString(getId()), Integer.toString(getIdCustomer()),
				Integer.toString(getIdFilm()), sdf.format(dateOfRental), dor };
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String dor;
		if(dateOfReturn==null)
			dor = "";
		else
			dor = sdf.format(dateOfReturn);
		return "Rental [id=" + id + ", customer=" + idCustomer + ", film=" + idFilm + ", dateOfRental="
				+ sdf.format(dateOfRental) + ", dateOfReturn=" + dor + "]";
	}

}
