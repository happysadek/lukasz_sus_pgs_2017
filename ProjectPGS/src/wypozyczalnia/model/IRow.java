package wypozyczalnia.model;

import java.io.Serializable;

/**
 * Interface pozwala klasie CollectionViewer wyswietlac wiersze prezentujace kolejne obiekty z listy.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 */
public interface IRow extends Serializable {
	public String[] getRow();
}
