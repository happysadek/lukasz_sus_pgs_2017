package wypozyczalnia.model;

/**
 * Klasa stanowi model filmu w wypozyczalni.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 */

public class Film implements IRow {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String title;
	private String genre;
	private String media;
	private boolean available;

	private static int LAST_ID = 0;

	private static int generateID() {
		LAST_ID += 1;
		return LAST_ID;
	}

	public Film(String title, String genre, String media) {
		super();
		this.id = generateID();
		this.title = title;
		this.genre = genre;
		this.media = media;
		this.available = true;
	}
	
	public Film(int id, String title, String genre, String media) {
		super();
		this.id = id;
		this.title = title;
		this.genre = genre;
		this.media = media;
		this.available = true;
	}
	
	public static int getLastId(){
		return LAST_ID;
	}
	
	public static void setLastId(int lastId){
		LAST_ID=lastId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}
	
	public boolean isAvailable(){
		return available;
	}
	
	public void setAvailability(boolean av){
		available = av;
	}

	@Override
	public String[] getRow() {
		return new String[] { Integer.toString(getId()), getTitle(), getGenre(), getMedia(), available ? "tak" : "nie" };
	}

	@Override
	public String toString() {
		return "Film [id=" + id + ", title=" + title + ", genre=" + genre + ", media=" + media + ", available=" + available + "]";
	}

}
