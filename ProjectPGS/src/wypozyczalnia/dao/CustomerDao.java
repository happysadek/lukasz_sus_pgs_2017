package wypozyczalnia.dao;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import wypozyczalnia.model.Customer;

/**
 * Klasa obsluguje liste klientow w wypozyczalni.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 */

public class CustomerDao implements Dao<Customer>{

	private static final long serialVersionUID = 1L;

	private final static Logger LOGGER = Logger.getLogger(CustomerDao.class.getName());

	private List<Customer> customers = new LinkedList<>();

	@Override
	public Customer add(Customer item) {
		customers.add(item);
		LOGGER.info("Added Customer: " + item);
		return item;
	}

	@Override
	public List<Customer> getAll() {
		return new LinkedList<>(customers);
	}

	@Override
	public Customer getOne(int id) {
		for (Customer customer : customers) {
			if (customer.getId() == id) {
				return customer;
			}
		}
		return null;
	}

	@Override
	public int count() {
		return customers.size();
	}

	@Override
	public void remove(int id) {
		Customer customer;
		for (Iterator<Customer> iter = customers.listIterator(); iter.hasNext();) {
			customer = iter.next();
			if (customer.getId() == id) {
				iter.remove();
				LOGGER.info("Removed Customer: " + customer);
				return;
			}
		}
	}

	@Override
	public void removeAll() {
		customers.clear();
	}

	@Override
	public List<Customer> find(Customer item) {
		List<Customer> filtered = new LinkedList<>(customers);
		List<Customer> tmp = new LinkedList<>();
		if(item.getId()!=-1){
			for(Customer i : filtered){
				if(i.getId()==item.getId())
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		if(!item.getName().equals("")){
			for(Customer i : filtered){
				if(i.getName().equals(item.getName()))
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		if(!item.getLastName().equals("")){
			for(Customer i : filtered){
				if(i.getLastName().equals(item.getLastName()))
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		if(item.getNumber()!=-1){
			for(Customer i : filtered){
				if(i.getNumber()==item.getNumber())
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		return filtered;
	}
}
