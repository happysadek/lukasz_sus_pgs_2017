package wypozyczalnia.dao;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import wypozyczalnia.model.Film;

/**
 * Klasa obsuguje liste filmow w wypozyczalni.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 */
public class FilmDao implements Dao<Film> {
	
	private static final long serialVersionUID = 1L;

	private final static Logger LOGGER = Logger.getLogger(FilmDao.class.getName());

	private List<Film> films = new LinkedList<>();

	@Override
	public Film add(Film item) {
		films.add(item);
		LOGGER.info("Added Film: " + item);
		return item;
	}

	@Override
	public List<Film> getAll() {
		return new LinkedList<>(films);
	}

	@Override
	public Film getOne(int id) {
		for (Film film : films) {
			if (film.getId() == id) {
				return film;
			}
		}
		return null;
	}

	@Override
	public int count() {
		return films.size();
	}

	@Override
	public void remove(int id) {
		Film film;
		for (Iterator<Film> iter = films.listIterator(); iter.hasNext();) {
			film = iter.next();
			if (film.getId() == id) {
				iter.remove();
				LOGGER.info("Removed Film: " + film);
				return;
			}
		}
	}

	@Override
	public void removeAll() {
		films.clear();
	}

	@Override
	public List<Film> find(Film item) {
		List<Film> filtered = new LinkedList<>(films);
		List<Film> tmp = new LinkedList<>();
		if(item.getId()!=-1){
			for(Film i : filtered){
				if(i.getId()==item.getId())
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		if(!item.getTitle().equals("")){
			for(Film i : filtered){
				if(i.getTitle().equals(item.getTitle()))
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		if(!item.getGenre().equals("")){
			for(Film i : filtered){
				if(i.getGenre().equals(item.getGenre()))
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		return filtered;
	}
}
