package wypozyczalnia.dao;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import wypozyczalnia.model.IRow;
import wypozyczalnia.model.Rental;

/**
 * Klasa obsluguje liste wypozyczen.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 */
public class RentalDao implements Dao<Rental>{
	
	private static final long serialVersionUID = 1L;

	private final static Logger LOGGER = Logger.getLogger(RentalDao.class.getName());

	private List<Rental> rentals = new LinkedList<>();

	@Override
	public Rental add(Rental item) {
		rentals.add(item);
		LOGGER.info("Added Rental: " + item);
		return item;
	}

	@Override
	public List<Rental> getAll() {
		return new LinkedList<>(rentals);
	}

	@Override
	public Rental getOne(int id) {
		for (Rental rental : rentals) {
			if (rental.getId() == id) {
				return rental;
			}
		}
		return null;
	}

	@Override
	public int count() {
		return rentals.size();
	}

	@Override
	public void remove(int id) {
		Rental rental;
		for (Iterator<Rental> iter = rentals.listIterator(); iter.hasNext();) {
			rental = iter.next();
			if (rental.getId() == id) {
				iter.remove();
				LOGGER.info("Removed Rental: " + rental);
				return;
			}
		}
	}

	@Override
	public void removeAll() {
		rentals.clear();
	}

	@Override
	public List<Rental> find(Rental item) {
		List<Rental> filtered = new LinkedList<>(rentals);
		List<Rental> tmp = new LinkedList<>();
		if(item.getId()!=-1){
			for(Rental i : filtered){
				if(i.getId()==item.getId())
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		if(item.getIdCustomer()!=-1){
			for(Rental i : filtered){
				if(i.getIdCustomer()==item.getIdCustomer())
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		if(item.getIdFilm()!=-1){
			for(Rental i : filtered){
				if(i.getIdFilm()==item.getIdFilm())
					tmp.add(i);
			}
			filtered.clear();
			filtered.addAll(tmp);
			tmp.clear();
		}
		return filtered;
	}
	
	public void getFinished(List<IRow> ren){
		for(Rental i : rentals){
			if(i.getDateOfReturn()!=null)
				ren.add(i);
		}
	}
	
	public void getNotFinished(List<IRow> ren){
		for(Rental i : rentals){
			if(i.getDateOfReturn()==null)
				ren.add(i);
		}
	}
}
