package wypozyczalnia.dao;

import java.util.List;
import java.io.Serializable;

/**
 * Dao = "Data Access Object"
 * Interface umozliwia dostep do danych i ich obsluge.
 * 
 * @author �ukasz Sus
 * @version 3.05.2017
 * @param <IRow> klasa przechowywanych danych
 */
public interface Dao<IRow> extends Serializable{

	IRow add(IRow item);
	
	List<IRow> getAll();
	
	IRow getOne(int id);
	
	int count();
	
	void remove(int id);
	
	void removeAll();

	List<IRow> find(IRow item);
}
